﻿using UnityEngine;
using System.Collections;

public class enemyAI : MonoBehaviour {
    public GameObject zhujue;
    public AudioSource ASqiangsheng;

    public bool isDead = false;
    Texture backupTexture = null;
    // Use this for initialization
    void Start () {
        
        backupTexture = gameObject.GetComponent<Renderer>().material.mainTexture;
    }



    public float chufafanwei = 30.0f;  //追踪范围
    public float attackfanwei = 20.0f; //攻击范围
                                // Update is called once per frame
    void Update () {

        if (transform.position.y < -60)
        {
            isDead = true;
        }

        if (!isDead)    //没死就上
        {
            //Debug.Log(Camera.main.GetComponent<viewSwitch>().isDead);
            if (!Camera.main.GetComponent<viewSwitch>().isDead)    //主角没死就上
            {
                //rotate();

                float distance = Vector3.Distance(zhujue.transform.position, transform.position);   //计算物体距离
                if (distance < chufafanwei && distance > attackfanwei)
                {
                    //gameObject.GetComponent<Renderer>().material.mainTexture = null;
                    //Debug.Log("i`m in move");
                    rotate();
                    move();
                    
                }
                else if (distance <= attackfanwei && distance > 5)
                {
                    //Debug.Log("i`m in move&attack");
                    rotate();
                    move();
                    attack();
                }
                else if (distance <= 5)
                {
                    //Debug.Log("i`m in rotate&attack");
                    rotate();
                    attack();
                }
                else
                {
                    gameObject.GetComponent<Renderer>().material.mainTexture = backupTexture;
                    //Debug.Log("i`m in nothing");
                }
            }
        }
        else
        {
            //gameObject.GetComponent<Renderer>().material.mainTexture = null;    //死了变白
        }
        

    }
    

    float x;    //游戏对象的x欧拉角
    float y;    //游戏对象的y欧拉角
    float z;    //游戏对象的y欧拉角
    float moveSpeed = 0.3f; //移速
    //float jumpPower = 3.0f;    //跳跃力量
    

    public float zhundu = 0.2f;    //射击准度，0~1之间的值,1是说百分之百命中，但是0不一定百分之百不中
                                   //从摄像机发射一条射线到鼠标位置(###不能射太准，没法玩)

    void move()
    {
        Vector3 angles = zhujue.transform.position - transform.position;//transform.eulerAngles; //计算位置 //zhujue.transform.eulerAngles - transform.eulerAngles;//

        transform.Translate(-angles * moveSpeed * Time.deltaTime, Space.Self);    //移动
        
    }
    void rotate()
    {
        //backupTexture = gameObject.GetComponent<Material>().GetTexture("yifu");
        Vector3 angles = zhujue.transform.position - transform.position;
        float angles2 = Vector3.Angle(angles, Vector3.forward);
        if (angles.x <= 0)  //角度矫正
        {
            angles2 = -1 * angles2;
        }
        else
        {

        }
        Quaternion rotation = Quaternion.Euler(0, angles2, 0);    //获取xy轴的欧拉角，人物不会左右倾斜，所以z轴欧拉角为0
        //Debug.Log("欧拉角：" + angles.y + "..." + angles2);

        this.transform.rotation = rotation;
        
    }

    public float shootFrequency = 1;    //射击频率
    private float shootTime;    //记录射击时的时间

    void attack()
    {
        float zhunduDistance = Vector3.Distance(zhujue.transform.position, transform.position); //距离
       // Debug.Log(Random.Range(zhunduDistance - (zhunduDistance - zhundu), zhunduDistance + (zhunduDistance - zhundu)));

        Vector3 attackPosition = (zhujue.transform.position - transform.position);
        //Ray ray = new Ray(transform.position, 
        //    new Vector3(
        //        attackPosition.x * Random.Range(zhunduDistance - (zhunduDistance - zhundu), zhunduDistance + (zhunduDistance - zhundu)), 
        //        attackPosition.y * Random.Range(zhunduDistance - (zhunduDistance - zhundu), zhunduDistance + (zhunduDistance - zhundu)),
        //        attackPosition.z * Random.Range(zhunduDistance - (zhunduDistance - zhundu), zhunduDistance + (zhunduDistance - zhundu))));//Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 shootVector = new Vector3(
                Random.Range(attackPosition.x - zhunduDistance * zhundu, attackPosition.x + zhunduDistance * zhundu),
                Random.Range(attackPosition.y - zhunduDistance * zhundu, attackPosition.y + zhunduDistance * zhundu),
                Random.Range(attackPosition.z - zhunduDistance * zhundu, attackPosition.z + zhunduDistance * zhundu));

        //Debug.Log("distance: " + zhunduDistance);
        if ((Time.time - shootTime)  > shootFrequency)
        {
            //Debug.Log("i`m play");
            //ASqiangsheng.Play();
            //Debug.Log(Time.time);

            //Debug.Log("i`m played");
            //Debug.Log("i`m shoot");

            Ray ray = new Ray(transform.position, shootVector);//Camera.main.ScreenPointToRay(Input.mousePosition);
            //Debug.Log("shootPos: " + shootVector.x + "..." + shootVector.y + "..." + shootVector.z);
            //游戏音效
            ASqiangsheng.Play();
            //gameObject.GetComponent<AudioSource>().Play();
            //AudioSource.PlayClipAtPoint(target.GetComponent<AudioClip>(), target.position);
            RaycastHit hitInfo;    //射线碰撞信息

            if (Physics.Raycast(ray, out hitInfo))    //发射射线，当发生碰撞时，将信息存储到hitInfo中。
            {
                //Debug.Log("i`m shoot in ");
                if (hitInfo.transform.gameObject.layer == 9)
                {
                    //Debug.Log("got it111");
                    int hitForce = 20;
                    //Debug.Log(hitInfo.transform.gameObject.layer);
                    //hitInfo.collider.gameObject.AddComponent<Rigidbody>(); 
                    
                    if (Camera.main.GetComponent<viewSwitch>().isDead == false)
                    {
                        //Debug.Log("got it222");
                        Camera.main.GetComponent<viewSwitch>().isDead = true;
                        Camera.main.GetComponent<viewSwitch>().ASdie.Play();
                        if (hitInfo.collider.gameObject.GetComponent<Rigidbody>())
                        {
                            //hitInfo.collider.gameObject.GetComponent<Renderer>().material.mainTexture = null;
                            hitInfo.collider.gameObject.GetComponent<Rigidbody>().AddRelativeForce(hitInfo.point * hitForce * Time.deltaTime, ForceMode.VelocityChange);
                            //Debug.Log("got it333..." + hitInfo.collider.gameObject.GetComponent<Rigidbody>());
                        }
                        else
                        {
                            //hitInfo.collider.gameObject.GetComponentInParent<Rigidbody>().AddForce(Vector3.forward, ForceMode.VelocityChange);
                            //Debug.Log("got it444");
                        }

                    }
                }
                else
                {

                }

            }

            shootTime = Time.time;
        }
        else
        {
            //等待不射击
        }        
        

    }
}
